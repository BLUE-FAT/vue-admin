import { login, getInfo } from "@/api/user";
import router, { resetRouter } from "@/router/index";

const state = {
  token: sessionStorage.getItem("token") ? sessionStorage.getItem("token") : "",
  roles: [],
  name: "",
  avatar: ""
};

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token;
    sessionStorage.setItem("token", token);
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles;
    sessionStorage.setItem("roles", JSON.stringify(roles));
  },
  SET_NAME: (state, name) => {
    state.name = name;
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar;
  }
};

const actions = {
  // user login
  login({ commit }, data) {
    return new Promise((resolve, reject) => {
      login(data)
        .then(response => {
          commit("SET_TOKEN", response.token);
          sessionStorage.setItem("refreshToken", response.refreshToken);
          sessionStorage.setItem("timestamp", response.timestamp);
          resolve();
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // get user info
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      getInfo()
        .then(response => {
          const { data } = response;
          if (!data) {
            return reject("验证失败，请重新登录");
          }

          const { roleKeys, name, avatar } = data.user;
          if (!roleKeys || roleKeys.length <= 0) {
            return reject("getInfo: roles must be a non-null array!");
          }
          commit("SET_ROLES", roleKeys);
          commit("SET_NAME", name);
          commit("SET_AVATAR", avatar);
          resolve(data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // user logout
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      commit("SET_TOKEN", "");
      commit("SET_ROLES", []);
      sessionStorage.clear();
      resetRouter();
      router.push("/login");
      resolve();
    });
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit("SET_TOKEN", "");
      commit("SET_ROLES", []);
      sessionStorage.clear();
      resetRouter();
      resolve();
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
