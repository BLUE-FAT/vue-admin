// import Layout from "@/components/layout/Layout.vue";
// import Layout from "@/components/layout/smart-layout-vertical";
import Layout from "@/components/layout/smart-layout";
import Router from "vue-router";
import Vue from "vue";

Vue.use(Router);

/**
 *
 * hidden: true                   在侧边栏隐藏
 * alwaysShow: true               在侧边栏显示
 * redirect: noRedirect           面包屑中不添加链接
 * name:'router-name'             用于 <keep-alive> 
 * meta : {
    roles: ['admin','editor']     异步路由的权限
    title: 'title'                侧边栏和面包屑的名字
    icon: 'svg-name'              侧边栏图标
    noCache: true                 不缓存页面
    affix: true                   在tab栏中常驻
    breadcrumb: false             不显示在面包屑
    activeMenu: '/example/list'   侧边栏选项高亮
  }
 */
export const constantRoutes = [
  {
    path: "/redirect",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/redirect/:path*",
        component: () => import("@/views/redirect/index")
      }
    ]
  },
  {
    path: "/login",
    component: () => import("@/views/Login"),
    hidden: true
  },
  {
    path: "/",
    component: Layout,
    redirect: "/homepage",
    children: [
      {
        path: "homepage",
        name: "homepage",
        component: () => import("@/views/Homepage"),
        meta: { title: "首页", icon: "el-icon-s-flag", affix: true }
      }
    ]
  },
  {
    path: "/telephone-book",
    component: Layout,
    redirect: "/telephone-book/index",
    meta: { title: "一级导航", icon: "el-icon-s-custom" },
    children: [
      {
        path: "index",
        name: "homepage",
        alwaysShow: true,
        meta: { title: "二级导航", icon: "el-icon-s-custom" },
        children: [
          {
            path: "index5",
            name: "homepage",
            component: () => import("@/views/Homepage"),
            meta: { title: "三级导航", icon: "el-icon-s-custom" }
          }
        ]
      },
      {
        path: "index2",
        name: "homepage",
        component: () => import("@/views/Homepage"),
        meta: { title: "二级导航", icon: "el-icon-s-custom" }
      },
      {
        path: "index3",
        name: "homepage",
        component: () => import("@/views/Homepage"),
        meta: { title: "二级导航", icon: "el-icon-s-custom" }
      }
    ]
  },
  {
    path: "/userInfo",
    component: Layout,
    redirect: "/userInfo/index",
    // hidden: true,
    children: [
      {
        path: "index",
        name: "userInfo",
        component: () => import("@/views/UserInfo"),
        meta: { title: "用户信息", icon: " el-icon-user-solid" }
      }
    ]
  },
  { path: "*", redirect: "/homepage", hidden: true }
];

export const asyncRoutes = [];
// 解决vue-router在3.0版本以上重复点菜单报错问题
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({
      y: 0
    }),
    routes: constantRoutes
  });

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
