import request from "@/assets/js/utils/request";
import axios from "axios"; //token续期

export function getPersonInfo(qid) {
  return request({
    url: "/api/mediationOfficer/getDetail",
    method: "get",
    params: {
      qid
    }
  });
}

//修改个人资料信息
export function savePersonInfo(data) {
  return request({
    url: "/api/mediationOfficer/saveUser",
    method: "post",
    data
  });
}
